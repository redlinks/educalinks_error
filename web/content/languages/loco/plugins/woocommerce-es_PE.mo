��    i      d      �      �  @   �     �  -   �     -     E      U     v     �     �     �  
   �  &   �  (   �          *  !   @     b     n     �     �  &   �     �     �  	   �  	   �     �     	     	     	     7	     D	     V	     ^	     l	     ~	     �	     �	  
   �	     �	  	   �	  	   �	  	   �	      
     
     
  8   0
     i
     u
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          %     -     J     Y  "   h     �     �     �     �     �     �     �  
   �     �          '     -     A     V     q     �     �     �     �     �     �  (   �                 	   2     <     I  	   W  )   a     �  
   �     �     �  )   �               &     >  �  K  <   E     �  1   �     �     �     �          +     F     T     n  1   z     �     �     �  "   �          )  	   @     J     P     l     }     �     �     �     �     �     �     �  	   �     �                    4     K     `     g     w     �     �     �     �     �  A   �          $     ,     4      <  
   ]  	   h     r     �     �     �     �     �     �     �     �     �     �     �  	   �     	          &  &   4     [      n     �     �     �     �     �     �  $   
  $   /  	   T  	   ^  
   h     s     |     �  #   �     �     �     �  
   �               %  %   2     X  	   o     y     �  ,   �     �     �     �     �   %s has been added to your cart. %s have been added to your cart. %s hour %s hours %s is a required field %s are required fields %s is a required field. %s is available %s is not a valid email address. %s is not a valid phone number. %s item %s items %s items %s items purchased %s product %s product updated %s products updated %s product updated. %s products updated. %s removed. &nbsp;&ndash; Page %s Add &ldquo;%s&rdquo; to your cart Add to cart Additional information Admin menu nameProducts All products Apartment, suite, unit etc. (optional) Billing details Cart Cart Page Cart page Cart totals Cart updated. Category Choose a country&hellip; Company name Continue shopping Country Email address Featured products Filter Products by Price Filter by category Filter by price First name House number and street name Last name Max price Min price Name Name (required) New Order: #{order_number} Notes about your order, e.g. special notes for delivery. Order notes Page settingCheckout Page slugcheckout Page titleCheckout Phone Phone Number Phone number. Postcode Postcode / ZIP Price Price: Prices Proceed to PayPal Proceed to checkout Product Product AttributeProduct %s Product Search Product price. Product quantity input tooltipQty Products Products totals. Quantity Regular price Related products Return to cart Return to shop Sale price Search products&hellip; Select a country&hellip; Share Sort by price (asc) Sort by price (desc) Sort by price: high to low Sort by price: low to high State State / County Street address Tag Terms and conditions Thank you for your order Thank you. Your order has been received. Town / City Undo? View &amp; Customize View cart View product View products View/Edit You cannot add another "%s" to your cart. Your cart is currently empty. Your order Your order was cancelled. [Remove] [{site_title}]: New order #{order_number} default-slugproduct min_priceFrom: placeholderBuy product slugproduct Project-Id-Version: WooCommerce 3.7.0
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
POT-Creation-Date: 2019-08-12 13:41:07+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-11-11 19:17+0000
Last-Translator: adminweb_links <adminweb@redlinks.com.ec>
Language-Team: Español de Perú
X-Generator: Loco https://localise.biz/
Language: es_PE
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.3.1; wp-5.2.3 %s ha sido agregado al pedido %s han sido agregado al pedido %s hora %s horas %s es un campo requerido %s son campos requeridos %s es un campo requerido %s esta disponible %s no es un email válido. %s no es un teléfono válido. %s artículo %s artículos %s artículos %s artículos solicitados %s producto %s producto actualizado %s productos actualizados %s producto actualizado.  %s ha sido eliminado. &nbsp;&ndash; Pagina %s Agregar &ldquo;%s&rdquo; al pedido Agregar al pedido Información Adicional Productos Todos número de local (opcional) Datos Personales Pedido Pedido Pedido Su Pedido es de: Pedido Actualizado. Categorías Elija un País... Centro Educativo o Academia Continuar País Email Promocionados Organizar productos por precio Filtrar por Categoría Organizar por precio Nombre Calle principal Apellido Precio Máximo Precio Mínimo Nombre Nombre (requerido) Nueva Orden: #{order_number} Detalle aquí sus consultas para hacer más específico su pedido  Comentarios Cotizar Cotizar Cotizar Teléfono convencional o celular Teléfono  Teléfono Código Postal Código postal Precio Precio: Precios Ir a PayPal Realizar Pedido Producto Producto %s Búsqueda de Producto Precio. Cantidad Productos Total de Productos. Cantidad Precio Normal Otros visitantes también eligieron... Regresar al Pedido Volver al catálogo de módulos  Precio de venta Buscar Productos... Seleccione un País... Compartir en ordenar por precio (asc) ordenar por precio (desc) ordenar por precio más alto primero ordenar por precio más bajo primero Provincia Provincia Dirección Etiqueta Términos y condiciones Gracias por su pedido Gracias, su orden ha sido recibida. Ciudad Deseas deshacer? Ver y Personalizar Ver Pedido Ver Producto Personalizar pedido Ver / Editar Nos es posible agregar "%s" al pedido Su pedido está vacio. Su pedido Su pedido fue cancelado. [Quitar] [{site_title}]: Nuevo Pedido #{order_number} producto Desde: Comprar Producto producto 