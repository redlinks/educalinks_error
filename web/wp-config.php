<?php

define('DB_NAME', 'educalinks'); // nombre de la base de datos.
define('DB_USER', 'root'); // nombre del usuario de la base de datos.
define('DB_PASSWORD', ''); // contraseña de la base de datos.
define('DB_HOST', 'localhost'); // Host MySQL

// The WordPress Security Keys - Las claves de seguridad de WordPress

define('AUTH_KEY',         'v{*h$p2--i.x2w2G.2X<%^|%+Ha]s:9T$*2->Ctw51pF7AWZFWj=W=I~E|L|OLQ;');
define('SECURE_AUTH_KEY',  'F7ikJ>yI1+U77_(cE7Ur;TC[/}B~V|b+5D]//8u5k(EL{FJyg@]Gbv=Q25N}gjM)');
define('LOGGED_IN_KEY',    'FNpPlx0K{P,KCZA>P*^RRA[cq^_2E8h<:!|^J`qu5Yz=+bI/|NuX.Iw6qvPR1skf');
define('NONCE_KEY',        '{v#vM`1~v8,pT<A:uHomiEaVF2CNFE%|mK&L+ULZpF9?.n3ey,nVzVJ8G82I<-dk');
define('AUTH_SALT',        '%vQ|[XV:;tZ3+ aV*g97F#edxD2!OBcQ$Y=IYC4R!_wE0b~J}F2$%x~y&L+NX$}E');
define('SECURE_AUTH_SALT', '@.]zsc?I!W{+MME|I&z,C}d!JscDW3$9~}QRrYV=)END=fB<mR.mF-}W6W-aiC@j');
define('LOGGED_IN_SALT',   'YWPI#R)h=fjax?[ppb^|T$!gu%kjrZuq=T_gySe0*SCyeL|YFXiqKZlb0 >KgzCQ');
define('NONCE_SALT',       'ZrzXC5xEC^/Zc|BnC9xwZ}^i&RKr?R}X[z|Y6p|Zf:<(ROVNdKX423o^Sa6|)WTM');

// The WordPress database table prefix - Prefijos de las tablas de la base de datos

$table_prefix  = 'wp_'; // solo números, letras y guión bajo al final.


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WPLANG', 'es_PE');

/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wordpress');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['HTTP_HOST'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content'); 
} 


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD','direct');